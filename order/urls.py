from django.conf.urls import patterns, url
from django.views.generic import TemplateView


urlpatterns = patterns('order.views',
    url('^add$', 'add', name='add_order'),
    url('^thank_you$', TemplateView.as_view(template_name='order/thank_you.html'), name='thank_you'),
)