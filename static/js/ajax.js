function fill_party(product_tag, product_id){
    // Загружает в одпции селекта партий для товара (product_id) доступные партии
    var party_name=product_tag.id.replace("product_label", "party");
    var party = $("#"+party_name);
    party.load("/products/search_party-"+product_id+"/");
}
function get_form_id(field){
    // Возвращает ID текущей формы
     return field.id.split('-', 3)[1]
}
function check_amount(field){
    // Сравнивает введенное значения количества со значением в теги остатоке
    // без проблем можно переделать на отправку запроса
    var form_id = get_form_id(field);
    var quantity_tag = $("#id_form-"+form_id+"-stock_quantity");
    var quantity_input = $("#id_form-"+form_id+"-quantity");
    var quantity_tag_err = $("#quantity_avail_err_"+form_id);
    if (parseInt(quantity_input.val()) > parseInt(quantity_tag.val())) {
        quantity_tag_err.show();
    } else (
        quantity_tag_err.hide()
    );
}
function get_quantity(field, product_id, party_id){
    // Отправляет AJAX запрос на количетсов товара(product_id) на складе с учетом партии (part_id)
    if  (party_id == undefined) {
        party_id = "";
    }
    var form_id = get_form_id(field);
    var quantity_tag = $("#id_form-"+form_id+"-stock_quantity");
    $.getJSON('/products/get_quantity-'+product_id+':'+party_id+'/').done(
        function(data){
            quantity_tag.val(data);
            check_amount(field);
        }
    );
}

$(document).ready(function(){
    $(".party_field").change(
        function(){
            var form_id = get_form_id(this);
            var product_id = $('#id_form-'+form_id+'-product').val();
            var party_id = $('#id_form-'+form_id+'-party').val();

            get_quantity(this, product_id, party_id);
        }
    );
    $(".quantity_field").change(
        function(){
            var form_id = get_form_id(this);
            var product_id = $('#id_form-'+form_id+'-product').val();
            var party_id = $('#id_form-'+form_id+'-party').val();
            get_quantity(this, product_id, party_id);
        }
    )
    }
);

