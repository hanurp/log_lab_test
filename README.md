== Подготовка ==

Зависимости:

pip

python-env

    sudo pip install virtualenv

== Устоновка ==

* Склонируйте из репозитария проект коммандой:

    $ git clone https://hanurp@bitbucket.org/hanurp/log_lab_test.git

* перехоим в каталог проекта

    $ cd log_lab_test

* создадим виртуальное окружение для проекта

    $ virtualenv --no-site-packages env

* Активируем

    $ source venv/bin/activate

* Устанавливаем зависимости в виртуальную среду

   $ pip install -r requirements

* Файл с тестовой БД (sqlite) с заполнеными данными уже есть в репозитарии

* запук теcтового сервера:

   $ python manage.ry runserver

* откройте в браузере url

 http://localhost:8000

* административный интерфейс

    http://localhost:8000/admin/
    пользователь: hanurp
    пароль: 4597

== Дополнительный команды ==

* Команда заполнения таблицы товаров(Product) с прайса на сайте http://nix.ru, принимает параметр:

 * --url с сылкой на страницу прайса (по умолчанию: http://www.nix.ru/price/price_list.html?section=notebooks_all)

    $ python manage.ry fill_products.py

Команда заполнения таблицы приходов(Balance) и партий(Party). Принимает параметры:

 * --party_amount - количество генерируемых партий. По умолчанию = 20

 * --clear_balances - очистить остатки

    $ python manage.ry fill_balances.py
