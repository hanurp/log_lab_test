from django.shortcuts import render, HttpResponseRedirect
from django.core.urlresolvers import reverse
# local
from .forms import AddOrderFormSet


def add(request, template_name='order/add.html'):
    if request.POST:
        add_order_formset = AddOrderFormSet(request.POST)
        if add_order_formset.is_valid():
            # do save
            return HttpResponseRedirect(reverse('thank_you'))
    else:
        add_order_formset = AddOrderFormSet()
    return render(request, template_name, {"add_order_formset": add_order_formset})
