# coding: utf-8
from django import forms
from django.forms.util import ErrorDict
from django.forms import widgets
from django.forms.formsets import formset_factory
from django.core.urlresolvers import reverse
from django.forms.formsets import BaseFormSet
from django.db.models import Sum
# project
from products.models import Product, Party
# local
from .widgets import AutocompleteInput


def add_errors(errors_form):
    """
    Добавляет ошибки в поля продукта во вс переданные формы
    """
    for error_form in errors_form:
        error_form._errors['quantity'] = u'Нет нужного количества на складе'
        del error_form.cleaned_data['quantity']


class BaseAddOrderFormSet(BaseFormSet):
    def clean(self):
        if any(self.errors):
            # Формы не прошли проверку по элементам
            return

        products_in_form = {}
        # Заполным данными из формсета в словарь вида =
        # {<product_id>:{
        #   total: <sum quantity of products in all forms >
        #   parties: {<party>: {sum:<sum quantity of products with party1 in all forms>,
        #                           forms:[]}}
        # }
        for form in self.forms:
            if not form.cleaned_data:
                continue

            quantity = (form.cleaned_data['quantity'])
            product_id = int(form.cleaned_data['product'] or 0)
            party_id = int(form.cleaned_data['party'] or 0)

            if product_id in products_in_form:
                info_dict = products_in_form[product_id]
                info_dict['total'] += quantity
                info_dict['forms'].append(form)
                parties = info_dict['parties']
                if party_id:
                    if party_id in parties:
                        parties[party_id]['sum'] += quantity
                    else:
                        parties[party_id]['sum'] = quantity
                    parties[party_id]['forms'].append(form)
            else:
                info_dict = {
                    'total': quantity,
                    'forms': [form],
                    'parties': {party_id: {'sum': quantity, 'forms': [form]}} if party_id else {},
                }
                products_in_form[product_id] = info_dict
        # Определим список всех id в формсете
        all_products = products_in_form.keys()
        # Получим словарь {product_pk: stock quantity}
        products_balance = Product.get_products_balance(all_products)
        # Получим словарь вида {product_pk:{party_id: stock_quantity_party}}
        parties_balance = Product.get_parties_balance(all_products)

        # проверка
        errors_form = []  # в этот список будим собирать фомы с ошибками
        for product_id, info_dict in products_in_form.iteritems():
            if info_dict['total'] > products_balance[product_id]:
                # по общему количеству
                add_errors(info_dict['forms'])
                errors_form.extend(info_dict['forms'])

            elif info_dict['parties']:
                # по партии
                for party_id, party_info in info_dict['parties'].iteritems():
                    if parties_balance[product_id][party_id] < party_info['sum']:
                        errors_form.extend(info_dict['forms'])
                        add_errors(info_dict['forms'])

        if errors_form:
            raise forms.ValidationError(u'Есть товар с недостающим количеством.')


class AddOrder(forms.Form):
    product = forms.CharField(label=u'')
    quantity = forms.IntegerField(label=u'', widget=widgets.TextInput(attrs={'class': 'quantity_field', 'size': '10'}))
    party = forms.CharField(label=u'', required=False, widget=widgets.Select(attrs={'class': 'party_field'}))
    stock_quantity = forms.CharField(label=u'', required=False, widget=widgets.TextInput(attrs={'readonly': 'true', 'size':'2', 'class': 'stock_field'}))

    def __init__(self, *args, **kwargs):
        super(AddOrder, self).__init__(*args, **kwargs)
        self.fields['product'].widget = AutocompleteInput(
            url=reverse('search_products'),
            obj_type=Product,
            attrs=dict(size=50)
        )
        data = kwargs.get('data')
        choices = [('', u'-------')]
        if data:
            prefix = kwargs.get('prefix')
            product_field_name = '%s%sproduct' % (prefix, prefix and '-' or '')
            product_id = int(data.get(product_field_name) or 0)
            if product_id:
                ch_list = [(item.id, u'%s' % item) for item in Party.objects.filter(balance__product__id=int(product_id))]
                choices.extend(ch_list)
        self.fields['party'].widget.choices = choices

                
AddOrderFormSet = formset_factory(AddOrder, formset=BaseAddOrderFormSet, extra=10)
