# python
from optparse import make_option
from BeautifulSoup import BeautifulSoup
import urllib
# django
from django.core.management.base import BaseCommand, CommandError
# log_lab
from products.models import Product


def get_small_title(title, max_words=3):
    # try to find '<'
    ind = title.find('&lt')
    if not ind == -1:
        return title[:ind].strip()
    return ' '.join(title.split(' ', max_words)[:-1])


class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--url',
                    dest='url',
                    default='http://www.nix.ru/price/price_list.html?section=notebooks_all',
                    help='Url of site  www.nix.ru with catalog'),
        make_option('--limit',
                    dest='limit',
                    default=1000,
                    help='Limit of products'),
    )

    def handle(self, *args, **options):
        url = options['url']
        limit = options['limit']
        added = 0
        f = urllib.urlopen(url)  # Open site for parsing
        soup = BeautifulSoup(f.read())  # Read it
        products = soup.findAll(name='a', attrs={'class': 't'})
        for p in products[:limit]:
            product_title = get_small_title(p.text)
            product, created = Product.objects.get_or_create(title=product_title)
            if created:
                added += 1
        self.stdout.write(u'Created = %s, All = %s.\n ' % (added, len(products)))