# python
import random
from optparse import make_option
# django
from django.core.management.base import BaseCommand, CommandError
# log_lab
from products.models import Product, Party, Balance


class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--party_amount',
                    dest='party_amount',
                    default=20,
                    help='The amount of parties to generate'),

        make_option('--clear_balances',
                    action='store_true',
                    dest='clear',
                    default=False,
                    help='Clear balances and parties'),
    )

    def handle(self, *args, **options):
        # delete old items
        clear = options['clear']
        if clear:
            Balance.objects.all().delete()
            Party.objects.all().delete()
            self.stdout.write(u'Clear successful.\n')
        # fill balances
        party_amount = int(options['party_amount'])
        if not party_amount:
            # not fill
            return None

        products_added = 0
        products = Product.objects.all().values_list('pk', flat=True)
        for p in xrange(party_amount):
            party = Party.objects.create()
            products_in_party = random.randint(5, 10)
            for n in xrange(products_in_party):
                product_quantity = random.randint(1, 5)
                product_pk = random.choice(products)
                Balance.objects.create(party=party, product_id=product_pk, quantity=product_quantity)
                products_added += 1

        self.stdout.write(u'Generate successful, party added {party_added} with {products_added} products.\n'.format(
            party_added=party_amount, products_added=products_added))