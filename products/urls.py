from django.conf.urls import patterns, url
from .views import SearchParty

urlpatterns = patterns('products.views',
    url('^search_products', 'search_products', name='search_products'),
    url('^search_party-(?P<product_id>[\d]+)/$', SearchParty.as_view(), name='search_party'),
    url('^get_quantity-(?P<product_id>[\d]+):(?P<party_id>[\d]*)/$', 'get_quantity', name='get_quantity'),
)