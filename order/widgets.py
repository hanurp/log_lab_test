# coding: utf-8
# django
from django.core.exceptions import ObjectDoesNotExist
# widgets.py
from django.forms.widgets import flatatt, TextInput
from django.template.loader import render_to_string


class AutocompleteInput(TextInput):
    """
    A form text input that gets rendered as an autocomplete widget using
    jQuery UI's autocomplete.
    http://jqueryui.com/autocomplete
    """

    def __init__(self, url, obj_type, attrs={}, extra_filter_tag='', max_rows=0, min_length=2, *args, **kwargs):
        """
        @param url: str. URL that returns JSON object, with options like this [{"value": pk, "label": "Omsk"}, ...]
        @obj_type:
        @extra_filter_tag: str. The id of tag which value contain extra filter param (e.g extra_filter_tag='country_state').        @can_add: bool. Allow users to set custom option. Default=False
        @max_rows: int. The maximum number of lines received from URL
        @min_length: int. The minimal length of text to send request to URL
        """
        super(TextInput, self).__init__(attrs=attrs)
        self.url = url
        self.obj_type = obj_type
        self.extra_filter_tag = extra_filter_tag
        self.max_rows = max_rows
        self.min_length = min_length
        self.attrs = attrs

    def render(self, name, value=None, template="order/widget_autocomlite.html", attrs={}):
        id_tag_attrs = self.build_attrs(attrs, name=name)

        if not 'id' in self.attrs:
            id_tag_attrs['id'] = 'id_{0}'.format(name)
            # создаем скрытый тэг для сохранения значения
        label_tag_attrs = dict(id_tag_attrs)
        label_tag_attrs['id'] += '_label'
        label_tag_attrs['name'] += '_label'

        if value:
            if isinstance(value, self.obj_type):
                id_tag_attrs['value'] = value.pk
                label_tag_attrs['value'] = value
            else:
                try:
                    label = self.obj_type.objects.get(pk=int(value))
                except ObjectDoesNotExist:
                    label = ''
                if label:
                    label_tag_attrs['value'] = label
                    id_tag_attrs['value'] = value

        return render_to_string(template, dict(
            id_tag_id=id_tag_attrs['id'],
            id_flat_attr=flatatt(id_tag_attrs),
            label_tag_id=label_tag_attrs['id'],
            label_flat_attr=flatatt(label_tag_attrs),
            url=self.url,
            max_rows=self.max_rows,
            min_length=self.min_length,
            extra_filter_tag=self.extra_filter_tag))
