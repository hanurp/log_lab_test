# coding: utf-8
from django.contrib import admin
# locale
from .models import Product, Party, Balance


class BalanceInline(admin.TabularInline):
    model = Balance
    extra = 0
    verbose_name = u'Партия'
    verbose_name_plural = u'Партии'


class ProductAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'get_stock_quantity',)
    inlines = [BalanceInline]

    class Meta:
        model = Product

admin.site.register(Product, ProductAdmin)
admin.site.register(Party)
admin.site.register(Balance)
