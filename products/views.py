# python
import json as simplejson
# django
from django.http import HttpResponse
from django.views.generic import ListView

# local
from .models import Product, Party


def http_response_JSON(context):
    context = simplejson.dumps(context, ensure_ascii=False)
    return HttpResponse(context, content_type='application/json')


def search_products(request):
    products = []
    if request.GET:
        term = str(request.GET.get('term'))
        if term:
            products = [{'label': u'%s' % item, 'value': u'%s' % item.id} for item in
                        Product.objects.filter(title__icontains=term.strip())]
    return http_response_JSON(products)


def get_quantity(request, product_id, party_id=None):
    product = Product.objects.get(pk=int(product_id))
    quantity = product.get_stock_quantity(party=party_id)
    return http_response_JSON(quantity)


class SearchParty(ListView):
    template_name = "order/ajaxselect.html"

    def get_queryset(self):
        return Party.objects.filter(balance__product__id=int(self.kwargs['product_id']))


