# coding: utf-8
from django.db import models


class Product(models.Model):
    title = models.CharField(max_length=300, verbose_name=u'Наименование', db_index=True)

    def __unicode__(self):
        return self.title.strip()

    @classmethod
    def get_products_balance(cls, products_ids):
        """
        Возвращает словарь с остатками товара на сладе для товаров в списке products_ids (за один запрос)
        @products_ids: список с id товаров для которых нужно определить остаток
        @return: {product_pk: stock quantity, ...}
        """
        qs = cls.objects.all()
        if products_ids:
            qs = qs.filter(id__in=products_ids)
        return dict(qs.values_list('pk').annotate(quantity=models.Sum('balance__quantity')))

    @classmethod
    def get_parties_balance(cls, products_ids):
        """
        Возвращает словарь с остатками товара на сладе для товаров в списке products_ids разбитый по партиям(за один запрос)
        @products_ids: список с id товаров для которых нужно определить остаток
        @return: {product_pk:{party_id: stock_quantity_party, ...}, ...}
        """
        qs = cls.objects.all()
        if products_ids:
            qs = qs.filter(id__in=products_ids)
        balance_party_seq = qs.values_list('pk', 'balance__party').annotate(models.Sum('balance__quantity'))
        balance_party = {}
        for product_id, party_id, quantity in balance_party_seq:
            if product_id in balance_party:
                balance_party[product_id].update({party_id: quantity})
            else:
                balance_party[product_id] = {party_id: quantity}
        return balance_party

    def get_stock_quantity(self, party=None):
        """
        Возвращает остаток данного товара на складе, если передан аргумент party то возвращает остаток только этой партии.
        @party: products.Party
        @return: int
        """
        # TODO: добавить коширование (только если не указана партия)
        qs = self.balance_set.all()
        if party:
            qs = qs.filter(party=party)
        stock_amount = qs.aggregate(sum=models.Sum('quantity')).get('sum') or 0
        return stock_amount

    class Meta:
        verbose_name = u'Товар'
        verbose_name_plural = u'Товары'


class Party(models.Model):
    datetime = models.DateTimeField(u'Дата и время создания партии', auto_now=True)

    def __unicode__(self):
        return u'№{0}'.format(self.pk)

    class Meta:
        verbose_name = u'Партия'
        verbose_name_plural = u'Партии'


class Balance(models.Model):
    product = models.ForeignKey(Product, verbose_name=u'Товар')
    party = models.ForeignKey(Party, verbose_name=u'Партия')
    quantity = models.IntegerField(u'Количество')

    class Meta:
        verbose_name = u'Таблица остатков'
        verbose_name_plural = u'Таблица остатков'