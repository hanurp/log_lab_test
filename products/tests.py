import json as simplejson
# coding: utf-8
from django.test import TestCase
from django.test.client import Client
from django.core.urlresolvers import reverse
# local
from .management.commands.fill_products import get_small_title
from .models import Product, Party, Balance


class ProductMethodsTestCase(TestCase):
    def setUp(self):
        """
        Создаем тестовый продукт P1 с общим количеством на складе 7шт (2шт в партии part1 и 5шт в party2).
        Создаем тестовый продукт P2 с нулевым остатком.
        Создаем тестовый продукт P3 с остатком 3шт(в партии party1) .
        """
        self.product1 = Product.objects.create(title='P1')
        self.product2 = Product.objects.create(title='P2')
        self.product3 = Product.objects.create(title='P3')
        self.party1 = Party.objects.create()
        self.party2 = Party.objects.create()
        self.balance1 = Balance.objects.create(product=self.product1, party=self.party1, quantity=2)
        self.balance2 = Balance.objects.create(product=self.product1, party=self.party2, quantity=5)
        self.balance3 = Balance.objects.create(product=self.product3, party=self.party1, quantity=3)

    def test_get_stock_quantity_p1(self):
        expected = 7
        result = self.product1.get_stock_quantity()
        self.assertEqual(result, expected)

    def test_get_stock_quantity_p1_with_party(self):
        expected = 2
        result = self.product1.get_stock_quantity(party=self.party1)
        self.assertEqual(result, expected)

    def test_get_stock_quantity_p2(self):
        expected = 0
        result = self.product2.get_stock_quantity()
        self.assertEqual(result, expected)

    def test_get_products_balances1(self):
        test_products = [self.product1.id, self.product3.id]
        expected = {self.product1.id: 7, self.product3.id: 3}
        result = Product.get_products_balance(test_products)
        self.assertDictEqual(result, expected)

    def test_get_products_balances2(self):
        test_products = [self.product1.id, self.product2.id]
        expected = {self.product1.id: 7, self.product2.id: None}
        result = Product.get_products_balance(test_products)
        self.assertDictEqual(result, expected)

    def test_get_products_balances3(self):
        test_products = [self.product2.id]
        expected = {self.product2.id: None}
        result = Product.get_products_balance(test_products)
        self.assertDictEqual(result, expected)


class ProductViewsTestCase(TestCase):
    def setUp(self):
        """
        Создаем тестовый продукт P1 с общим количеством на складе 7шт (2шт в партии part1 и 5шт в party2).
        Создаем тестовый продукт P2 с нулевым остатком.
        Создаем тестовый продукт P3 с остатком 3шт(в партии party1) .
        """
        self.product1 = Product.objects.create(title='Sony X1')
        self.product2 = Product.objects.create(title='Sony X2')
        self.product3 = Product.objects.create(title='Apple MAC')
        self.party1 = Party.objects.create()
        self.party2 = Party.objects.create()
        self.balance1 = Balance.objects.create(product=self.product1, party=self.party1, quantity=2)
        self.balance2 = Balance.objects.create(product=self.product1, party=self.party2, quantity=5)
        self.balance3 = Balance.objects.create(product=self.product3, party=self.party1, quantity=3)
        self.client = Client()

    def test_search_products(self):
        response = self.client.get(reverse('search_products'), {'term': 'Sony'})
        expected = 2
        result = len(simplejson.loads(response.content))
        self.assertEqual(result, expected)


class ManagementTestCase(TestCase):
    def test_fill_products_get_small_title_1(self):
        expect = u'Acer Aspire E1-572G-74506G50Mnkk'
        result = get_small_title(u'Acer Aspire E1-572G-74506G50Mnkk &lt;NX.M8KER.003&gt; i7 4500U / 6 / 500 / DVD-RW / HD8670M / WiFi / BT / Win8 / 15.6&quot; / 2.24 \xea\xe3')
        self.assertEqual(result, expect)

    def test_fill_products_get_small_title_2(self):
        expect = u'Acer Aspire E1-572G-74506G50Mnkk'
        result = get_small_title(u'Acer Aspire E1-572G-74506G50Mnkk NX.M8KER.003&gt; i7 4500U / 6 / 500 / DVD-RW / HD8670M / WiFi / BT / Win8 / 15.6&quot; / 2.24 \xea\xe3')
        self.assertEqual(result, expect)